FROM mcr.microsoft.com/dotnet/sdk:2.1-focal


# To make it easier for build and release pipelines to run apt-get,
# configure apt to not require confirmation (assume the -y argument by default)
ENV DEBIAN_FRONTEND=noninteractive
RUN echo "APT::Get::Assume-Yes \"true\";" > /etc/apt/apt.conf.d/90assumeyes

RUN apt-get update \
&& apt-get install -y --no-install-recommends \
        dirmngr \
        gnupg \
        apt-transport-https \
        ca-certificates \
        curl 
RUN curl -sSL 'http://keyserver.ubuntu.com/pks/lookup?op=get&search=0xa6a19b38d3d831ef' | apt-key add -
RUN sh -c 'echo "deb https://download.mono-project.com/repo/ubuntu stable-focal main" > /etc/apt/sources.list.d/mono-official-stable.list'

RUN apt-get update \
&& apt-get install -y  \
        mono-complete
RUN curl -o /usr/local/bin/nuget.exe https://dist.nuget.org/win-x86-commandline/latest/nuget.exe

RUN echo 'alias nuget="mono /usr/local/bin/nuget.exe"' >> ~/.bashrc

